from django.shortcuts import render
from .models import Hat, LocationVO
from common.json import ModelEncoder
import json
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods

# Create your views here.
class LocationVODetailEncoder(ModelEncoder):
    model=LocationVO
    properties = [
        "closet_name",
        "section_number",
        "shelf_number",
        "import_href",
        "id"
    ]

class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style_name",
        "color",
        "picture",
        "id",
    ]

    encoders = {
        "location": LocationVODetailEncoder()
    }
    def get_extra_data(self, o):
        return {"location": o.location.closet_name}



class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style_name",
        "color",
        "picture",
        "id",
        "location"
    ]

    encoders = {
        "location": LocationVODetailEncoder()
    }

@require_http_methods(["GET","POST"])
def api_list_hats(request):
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats":hats},
            encoder=HatListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            location_href = content["location"]
            location=LocationVO.objects.get(import_href=location_href)
            content["location"] = location

        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location name"},
                status=400
            )

        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )

@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_hat(request, id):
    if request.method == "GET":
        try:
            hat = Hat.objects.get(id=id)
            return JsonResponse(
                hat,
                encoder=HatDetailEncoder,
                safe=False
            )
        except Hat.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "PUT":
        content = json.loads(request.body)
        Hat.objects.filter(id=id).update(**content)
        hat = Hat.objects.get(id=id)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )
    else: #Delete
        count, _ = Hat.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})



# @require_http_methods(["GET", "POST"])
# def api_list_location_vo(request):
#     if request.method == "GET":
#         locations = LocationVO.objects.all()
#         return JsonResponse(
#             {"locationVOs": locations},
#             encoder = LocationVODetailEncoder
#         )

import React from 'react'

async function Hatdelete(hat) {
    const hatUrl = `http://localhost:8090/api/hats/${hat.id}`;
    const fetchConfig = {
        method: "delete",
        headers: {
        'Content-Type': 'application/json',
        },
    };
    await fetch(hatUrl, fetchConfig);
    window.location.reload(true);

}

function HatList(props) {
  return (
    <table className="table table-striped">
        <thead>
          <tr>
            <th>Style Name</th>
            <th>Fabric</th>
            <th>Color</th>
            <th>Location</th>
          </tr>
        </thead>
        <tbody>
          {props.hats.map(hat => {
            console.log(hat)
            return (
              <tr key = {hat.id}>
                <td>{ hat.fabric }</td>
                <td>{ hat.style_name }</td>
                <td>{ hat.color }</td>
                <td>{ hat.location }</td>
                <td>
                    <img
                        src={hat.picture}
                        width="70"
                        height="70"
                        />
                </td>
                <td>
                    <button className="btn btn-danger" onClick={() => Hatdelete(hat)}>Delete Hat</button>
                </td>
              </tr>
            );
          })}

        </tbody>
      </table>
  )
}



export default HatList

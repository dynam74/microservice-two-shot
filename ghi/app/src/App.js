import { BrowserRouter, Routes, Route } from 'react-router-dom';
import HatForm from './HatForm';

import HatList from './HatList';
import MainPage from './MainPage';
import ShoeForm from './ShoeForm';
import ShoeList from './ShoeList';
import Nav from './Nav';


function App(props) {
  if (props.hats === undefined && props.shoes===undefined) {
    return null
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route index element={<MainPage />} />
          <Route path="shoes">
            <Route path="new" element={<ShoeForm />} />
            <Route path="" element={<ShoeList shoes={props.shoes} />} />
          </Route>
          <Route path="hats">
            <Route path="" element={<HatList hats={props.hats} />} />
            <Route path="new" element={<HatForm/>}/>
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;

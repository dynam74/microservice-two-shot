import React from 'react'


    async function deleteShoe(shoe) {
    const shoeUrl= `http://localhost:8080/api/shoes/${shoe.id}`;
    const fetchConfig ={
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json'
        },
    };
    await fetch(shoeUrl, fetchConfig);
    window.location.reload(true);
        
    }
    function ShoeList(props){
    return (
        <div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Manufacturer</th>
                        <th>Model Name</th>
                        <th>Color</th>
                        <th>Picture</th>
                        <th>Bin</th>
                        <th></th>
                    </tr>
                </thead>
                 <tbody>
                    {props.shoes.map(shoe => {
                        return (
                            <tr key={shoe.id}>
                                <td>{shoe.manufacturer}</td>
                                <td>{shoe.model_name}</td>
                                <td>{shoe.color}</td>
                                <td>
                                    <img src={shoe.picture_URL} alt="" width="100px" height="100px"/>
                                </td>
                                <td>{shoe.bin}</td>
                                {<td>
                                    <button className="btn btn-danger" onClick={() => deleteShoe(shoe)}>Delete Shoe</button>
                                </td>}
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    )
}

export default ShoeList



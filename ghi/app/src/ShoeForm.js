import React,  {useEffect, useState} from 'react';

function ShoeForm(props){
  const [bins, setBins] = useState([]);
  const [manufacturer, setManufacturer] = useState('');
  const [modelName, setModelName] = useState('');
  const [color, setColor] = useState('')
  const [pictureURL, setPictureURL] = useState ('')
  const [bin, setBin] = useState('')

  
  const handleManufacturerChange = (event) => {
    const value = event.target.value;
    setManufacturer(value);
  }
  const handleModelNameChange = (event) => {
    const value = event.target.value
    setModelName(value)
  }
  const handleColorChange = (event) => {
    const value = event.target.value
    setColor(value)
  }
  const handlePictureURLChange = (event) => {
    const value = event.target.value
    setPictureURL(value)
  }
  const handleBinChange = (event) => {
    const value = event.target.value
    setBin(value)
  }
  const handleSubmit = async (event) => {
    event.preventDefault();
  
    // create an empty JSON object
    const data = {}
    data.model_name = modelName;
    data.picture_URL = pictureURL;
    data.manufacturer = manufacturer;
    data.color = color
    data.bin = bin

    const shoeUrl = "http://localhost:8080/api/shoes/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const shoeResponse = await fetch(shoeUrl, fetchConfig);
        if (shoeResponse.ok) {
            const newShoe = await shoeResponse.json();
            console.log(newShoe);
              setManufacturer('')
              setModelName('')
              setColor('')
              setPictureURL('')
              setBin('')
        }
    }
    const fetchData = async () => {
        const url = "http://localhost:8100/api/bins/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setBins(data.bins);
        }
    }
    useEffect(() => {
        fetchData();
      }, []);
      return (
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a new shoe</h1>
                <form onSubmit={handleSubmit} id="create-shoe-form">
                <div className="form-floating mb-3">
                    <input onChange={handleManufacturerChange} placeholder="Manufacturer" 
                    required type="text" name="name" 
                    id="manufacturer" className="form-control" value={manufacturer}/>
                    <label htmlFor="manufacturer">Manufacturer</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleModelNameChange} placeholder="ModelName" 
                    required type="text" name="modelName" 
                    id="modelName" className="form-control" value={modelName}/>
                    <label htmlFor="modelName">Model Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleColorChange} placeholder="Color" required 
                    type="text" name="color" id="color" className="form-control" value={color}/>
                    <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handlePictureURLChange} className="form-control" name="picture" 
                    placeholder="Picture URL" id="picture" required type="text"value={pictureURL}/>
                    <label htmlFor="picture_url">Picture URL</label>
                </div>
                <div className="mb-3">
                    <select onChange={handleBinChange} required name="bin" 
                    className="form-select" id="bin">
                    <option value="">Choose a Bin</option>
                    {bins.map(bin => {
                        return (
                            <option key={bin.id} value={bin.href}> {bin.closet_name} </option>
                        );
                    })}
                    </select>
                </div>
                <button className="btn btn-primary">Create</button>
                </form>
            </div>
            </div>
        </div>
    );
}


export default ShoeForm;

    

  

  
  

